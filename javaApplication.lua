-- Here we set a number of local variables that describe the application
-- this module is for, which version of the application
-- if it was built with a compiler or which language it uses ( CompilerLanguage = cl)
-- and the version of this cl.
local appName = "Java App Name"
local appFolder = "folder name"
local version = "x.x.x"
local cl = "java"
local clVersion = "1.8.0_66"


-- List any prerequisites for this application
load("java/1.8.0_66")


-- List any conflicting Apps
-- conflict("name1", "name2")


-- Here we build up the env vars to be exported
local classpath = pathJoin( "/usr/local/installed", appFolder, version )


-- Here we set the appname directory vars to the environment
setenv( "__APPNAME__DIR", classpath )


-- display usage information
if( mode() == "load" ) then
  LmodMessage( "\nTo use ___APPNAME___ you need to call java -jar $__APPNAME__DIR/__APP__.jar ....\n" )
end


-- help
help( [[This module loads ]] .. appName .. [[ v.]] .. version .. [[

Any information about the java application

--Environment Variables Available--
$__APPNAME__DIR - Install folder for application
$__OTHERS__ - <quick description>
]])


-- Short Description of App
whatis( "Description: app: " .. appName )