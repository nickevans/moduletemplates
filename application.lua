-- Here we set a number of local variables that describe the application
-- this module is for, which version of the application
-- if it was built with a compiler or which language it uses ( CompilerLanguage = cl)
-- and the version of this cl.
local appName = "Application Name"
local appFolder = "folderName"
local version = "x.x"
local cl = "compiler/language"
local clVersion = "x.x.x"


-- List any prerequisites for this application
-- load("compiler1","lib2","app1","lib4")


-- List any conflicting Apps
-- conflict("name1", "name2")


-- Here we build up the env vars to be exported
local base = pathJoin( "/usr/local/installed", appFolder, version )
-- local base = pathJoin( "/usr/local/installed", appFolder, version, cl .. "-" .. clVersion )
local bin = pathJoin( base, "bin" )
local include = pathJoin( base, "include" )
local lib = pathJoin( base, "lib" )
local share = pathJoin( base, "share" )
local man = pathJoin( share, "man" )
local cppFlags = "-I" .. include
local ldFlags = "-L" .. lib


-- Here we prepend our built up vars to the environment
prepend_path( "PATH", bin )
prepend_path( "LIBRARY_PATH", lib )
prepend_path( "LD_LIBRARY_PATH", lib )
prepend_path( "MANPATH", man )
prepend_path( "CPPFLAGS", cppFlags, " " )
prepend_path( "LDFLAGS", ldFlags, " " )


-- set base folder environment variable for application 
setenv( "__APPNAME__DIR", base )


-- help
help( [[This module loads ]] .. appName .. [[ v.]] .. version .. [[

Any information that should be advised when a user uses help

--Environment Variables Available--
$__APPNAME__DIR - Install folder for application
$__OTHERS__ - <quick description>
]])


-- Short Description of App
whatis( "Description: app: " .. appName )


-- prepend path with alternate delimiter
-- prepend_path( "ENV_VAR", localVar, "Delimiter" )
-- prepend_path( "CPPFLAGS", cppFlags, " " )
-- prepend_path( "LDFLAGS", ldFlags, " " )
-- Get env var

-- local localVar = os.getenv("ENV_VAR")

-- Check if var has content
-- if localVar == nil then
--  do stuff
-- else
--  do other stuff
-- end

-- Set environment variable
-- setenv( "ENV_VAR", localVar )