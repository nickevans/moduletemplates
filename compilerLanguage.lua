-- Here we set a number of local variables that describe the application
-- this module is for, which version of the application
-- if it was built with a compiler or which language it uses ( CompilerLanguage = cl)
-- and the version of this cl.
local name = "Compiler / Language"
local folder = "folderName"
local version = "x.x"


-- List any prerequisites
-- load("compiler1","lib2","app1","lib4")


-- List any conflicting Apps
-- conflict("name1", "name2")


-- Here we build up the env vars to be exported
local base = pathJoin( "/usr/local/installed", folder, version )
local bin = pathJoin( base, "bin" )
local include = pathJoin( base, "include" )
local lib = pathJoin( base, "lib" )
local lib64 = pathJoin( base, "lib64" )
local share = pathJoin( base, "share" )
local man = pathJoin( share, "man" )
local cppFlags = "-I" .. include
local ldFlags = "-L" .. lib


-- Here we prepend and export our built up vars to the environment
prepend_path( "PATH", bin )
prepend_path( "LIBRARY_PATH", lib )
prepend_path( "LD_LIBRARY_PATH", lib )
prepend_path( "MANPATH", man )
prepend_path( "CPPFLAGS", cppFlags, " " )
prepend_path( "LDFLAGS", ldFlags, " " )


-- set base folder environment variable for language 
setenv( "__LANGUAGENAME__DIR", base )


-- help
help( [[This module loads ]] .. appName .. [[ v.]] .. version .. [[

Any information that should be advised when a user uses help

--Environment Variables Available--
$__LANGUAGENAME__DIR - Install folder for language
$__OTHERS__ - <quick description>
]])


-- Short Description of App
whatis( "Description: app: " .. appName )


-- This section instructs lmod to look for more application that are now available
-- due to the added functionality
-- DON'T EDIT BELOW UNLESS YOU KNOW WHAT YOU ARE DOING
local mroot = os.getenv("MODULEPATH_ROOT")
local mdir = pathJoin(mroot, folder, version)
prepend_path("MODULEPATH", mdir)